# Bus Reporting

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Running the application

Once this repository has been cloned using the
git clone https://bitbucket.org/ashkolyar/tfnsw-code-challenge.git command.

run npm install to install all the node modules and dependencies.
which should also install angular/cli.

the app can then be run and tested with the commands listed below.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Assumptions

1. Multiple notes could be added edited and deleted, which has been implemented.
   These notes are only stored in memory and not persisted to any back-end.

2. Status is derived from the deviationFromTimetable based on the supplied example png
   I have made the assumption that negative values correspond to a status of 'Early'
   possitive values > 300 correspond to a status of 'Late'
   values between 0 and 300 correspond to a status of 'On Time'
   null values correspond to a status of 'Unknown'

Test have been written to test that logic.

Please make sure to run the karma and the e2e tests using the above commands
i.e. `ng test`, and `ng e2e`
