import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusReportsComponent } from './bus-reports.component';
import { CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA } from '@angular/core';
import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { BusItem } from './bus-item';
import { BusReport } from '../bus.report';
import { BusNote } from './note';

describe('BusReportsComponent', () => {
  let component: BusReportsComponent;
  let fixture: ComponentFixture<BusReportsComponent>;
  let busReports : BusReport;
  let pos : number = 0;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusReportsComponent ],
      imports: [NgbAccordionModule.forRoot(),HttpClientModule],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusReportsComponent);
    component = fixture.componentInstance;
    busReports = new BusReport();
    busReports.data = new Array<BusItem>();
    busReports.data.push(new BusItem());
    busReports.data[0].busNotes = new Array<BusNote>();
    component.busReports = busReports;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should save note', () => {
      //create a BusItem object
      let busItem : BusItem = new BusItem();
      //set the note property
      busItem.note = 'testing 123';

      //call onSave passing the busItem and the position of the
      //bus reports data array
      //bus report can have multiple data objects and each one can have its own notes section
      component.onSave(busItem, pos);

      //there should be 1 item in the notes section for that data item
      expect(component.busReports.data[pos].busNotes.length).toBe(1);

      let busNote : BusNote = component.busReports.data[pos].busNotes[0];
      expect(busNote.note).toBe('testing 123');
   
  });

  it('should update note', () => {
    let busItem : BusItem = new BusItem();
    busItem.busNotes  = new Array<BusNote>();
    busItem.note = 'testing 123';
 
    component.onSave(busItem, pos);

    expect(component.busReports.data[pos].busNotes.length).toBe(1);

    let busNote : BusNote = component.busReports.data[pos].busNotes[0];
    expect(busNote.note).toBe('testing 123');
    expect(busNote.noteId).toBe(1);
    
    //create a new note 
    let newBusItem : BusItem = new BusItem();
    newBusItem.busNotes = new Array<BusNote>();
    newBusItem.note = 'This is another note';

    component.onSave(newBusItem,pos);
    //there should be 2 now
    expect(component.busReports.data[pos].busNotes.length).toBe(2);

    //update the note for the first busitem
    busItem.note = 'Updated Note Data';

    //set the selectedBusNoteId as this is what used to test whether
    //a note is being added or edited
    component.selectedBusNoteId = busNote.noteId;
    component.onSave(busItem, pos);
    
    let updatedBusNote : BusNote = component.busReports.data[pos].busNotes[0];
    expect(updatedBusNote.note).toBe('Updated Note Data');
    expect(updatedBusNote.noteId).toBe(1);
    expect(updatedBusNote.modifiedDate).toBeDefined;

  });

  it('should move new notes to the top of the notes array', () => {
    let busItem1 : BusItem = new BusItem();
    busItem1.busNotes  = new Array<BusNote>();
    busItem1.note = 'note 1';
 
    component.onSave(busItem1, pos);

    //create a new note 
    let busItem2 : BusItem = new BusItem();
    busItem2.busNotes = new Array<BusNote>();
    busItem2.note = 'note 2';

    component.onSave(busItem2,pos);

    //create a new note 
    let busItem3 : BusItem = new BusItem();
    busItem3.busNotes = new Array<BusNote>();
    busItem3.note = 'note 3';

    component.onSave(busItem3,pos);

    expect(component.busReports.data[pos].busNotes.length).toBe(3);

    let lastNoteAdded : BusNote = component.busReports.data[pos].busNotes[0];
    expect(lastNoteAdded.note).toBe('note 3');
    expect(lastNoteAdded.noteId).toBe(3);
  });

  it('should move existing updated notes to the top of the notes array', () => {
    let busItem1 : BusItem = new BusItem();
    busItem1.busNotes  = new Array<BusNote>();
    busItem1.note = 'note 1';
 
    component.onSave(busItem1, pos);

    //create a new note 
    let busItem2 : BusItem = new BusItem();
    busItem2.busNotes = new Array<BusNote>();
    busItem2.note = 'note 2';

    component.onSave(busItem2,pos);

    //create a new note 
    let busItem3 : BusItem = new BusItem();
    busItem3.busNotes = new Array<BusNote>();
    busItem3.note = 'note 3';

    component.onSave(busItem3,pos);

    expect(component.busReports.data[pos].busNotes.length).toBe(3);

    let lastNoteAdded : BusNote = component.busReports.data[pos].busNotes[0];
    expect(lastNoteAdded.note).toBe('note 3');
    expect(lastNoteAdded.noteId).toBe(3);

    //the first note added should be the last note 
    let firstNoteAdded : BusNote = component.busReports.data[pos].busNotes[2];
    expect(firstNoteAdded.note).toBe('note 1');
    expect(firstNoteAdded.noteId).toBe(1);

    component.selectedBusNoteId = firstNoteAdded.noteId;
    busItem1.note = 'note 1 updated';
    component.onSave(busItem1, pos);

    let updatedNote : BusNote = component.busReports.data[pos].busNotes[0];
    expect(updatedNote.noteId).toBe(firstNoteAdded.noteId);
    expect(updatedNote.note).toBe('note 1 updated');

  });
});
