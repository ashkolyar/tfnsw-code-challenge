export class BusNote {
    private _modifiedDate :Date;
    constructor(private id:number, private _note: string, private entered:Date) { 

    }

    set note(note: string) {
        this._note = note;
    }

    set modifiedDate(modifiedDate : Date){
        this._modifiedDate = modifiedDate;
    }

    get modifiedDate() : Date{
        return this._modifiedDate;
    }

    get note() : string {
        return this._note;
    }

    get noteId() : number {
        return this.id;
    }

}