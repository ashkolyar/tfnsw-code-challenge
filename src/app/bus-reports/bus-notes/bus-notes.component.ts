import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BusItem } from '../bus-item';
import { BusNote } from '../note';
import { BusReport } from '../../bus.report';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';

@Component({
  selector: 'app-bus-notes',
  templateUrl: './bus-notes.component.html',
  styleUrls: ['./bus-notes.component.scss']
})
export class BusNotesComponent implements OnInit {
  @Input() busItem: BusItem;
  @Input() busReports: BusReport;
  @Output() onNoteSelected: EventEmitter<any>;
  @Input() selectedBusNoteId : number;

  constructor(private confirmationDialogService: ConfirmationDialogService) {
    this.onNoteSelected = new EventEmitter();
  }

  ngOnInit() {
  }

  openConfirmationDialog(busItem: BusItem, busNote: BusNote) {
    this.confirmationDialogService.confirm('Delete Note..', 'Do you really want to do this... ?')
    .then((confirmed) => {confirmed ? this.onDelete(busItem,busNote) : null})
    .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }


  onEdit(busItem: BusItem, busNote: BusNote): void {

    let item = null;

    for (let i = 0; i < this.busReports.data.length; i++) {
      item = this.busReports.data[i];

      if (item.organisation === busItem.organisation) {
        
        item.note = busNote.note;

        this.onNoteSelected.emit(busNote.noteId);
        break;
      }
    }
  }

  onDelete(busItem: BusItem, busNote: BusNote): void {
    let item = null;

    for (let i = 0; i < this.busReports.data.length; i++) {
      item = this.busReports.data[i];

      if (item.organisation === busItem.organisation) {
        break;
      }
    }

    if (item !== null) {
      let pos: number = -1;
      for (let j = 0; j < item.busNotes.length; j++) {
        if (busNote.noteId === item.busNotes[j].noteId) {
          pos = j;
          break;
        }
      }

      if (pos !== -1) {
        //remove the item from the array using splice
        item.busNotes.splice(pos, 1);
      }

      //if no items in array set it to null so that notes header is removed from display
      if (item.busNotes.length === 0) {
        item.busNotes = null;
      }
    }
  }
}
