import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BusNotesComponent } from './bus-notes.component';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';

import { CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA } from '@angular/core';

import { BusReport } from '../../bus.report';
import { BusItem } from '../bus-item';
import { BusNote } from '../note';


describe('BusNotesComponent', () => {
  let component: BusNotesComponent;
  let fixture: ComponentFixture<BusNotesComponent>;
  let confirmationDialogService : ConfirmationDialogService;
  let busReports : BusReport;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusNotesComponent],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA
      ],
      providers:[ConfirmationDialogService],
      imports: [NgbModule.forRoot()] 
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusNotesComponent);
    component = fixture.componentInstance;
    confirmationDialogService = TestBed.get(ConfirmationDialogService);

    busReports = new BusReport();
    busReports.data = new Array<BusItem>();
    
    let sydneyBuses : BusItem = new BusItem();
    sydneyBuses.organisation = 'Sydney Buses';
    sydneyBuses.busNotes = new Array<BusNote>();
    busReports.data.push(sydneyBuses);

    let westBus : BusItem = new BusItem();
    westBus.organisation = 'Westbus';
    westBus.busNotes = new Array<BusNote>();
    busReports.data.push(westBus);

    component.busReports = busReports;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should delete selected note', () => {
    //create some notes first
    let busNote1 : BusNote = new BusNote(1,'note 1', new Date());
    let busNote2 : BusNote = new BusNote(2,'note 2', new Date());
    let busNote3 : BusNote = new BusNote(3,'note 3', new Date());
    
    //add notes to the first busItem aka sydney buses
    component.busReports.data[0].busNotes.push(busNote3);
    component.busReports.data[0].busNotes.push(busNote2);
    component.busReports.data[0].busNotes.push(busNote1);
   
    expect(component.busReports.data[0].busNotes.length).toBe(3);

    //add notes to the second busItem aka westbus
    component.busReports.data[1].busNotes.push(busNote3);
    component.busReports.data[1].busNotes.push(busNote2);

    expect(component.busReports.data[1].busNotes.length).toBe(2);


    //create a busItem and set the organisation as this is used to find the right notes array
    let busItemToDelete : BusItem = new BusItem();
    busItemToDelete.organisation = 'Sydney Buses';

    component.onDelete(busItemToDelete,busNote3);
    expect(component.busReports.data[0].busNotes.length).toBe(2);

    component.onDelete(busItemToDelete,busNote2);
    expect(component.busReports.data[0].busNotes.length).toBe(1);
    
    component.onDelete(busItemToDelete,busNote1);
    expect(component.busReports.data[0].busNotes).toBeNull;

    //busItem 2's aka westbus notes should not have been deleted
    expect(component.busReports.data[1].busNotes.length).toBe(2);
  });

  it('should emit the selectedNoteId', (done) => {
    //the selectedNoteId that is emited by this eventEmitter
    //when onEdit is called should be 1 as that is the id of the Note
    //being passed in
    component.onNoteSelected.subscribe(g => {
       expect(g).toEqual(1);
       done();
    });

    let sydneyBuses : BusItem = new BusItem();
    sydneyBuses.organisation = 'Sydney Buses';

    let busNote1 : BusNote = new BusNote(1,'note 1', new Date());

    component.onEdit(sydneyBuses,busNote1);
  });
});
