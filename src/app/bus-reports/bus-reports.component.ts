import { Component, OnInit  } from '@angular/core';
import { BusReport } from '../bus.report';
import { BusService } from '../bus.service';
import { NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { BusNote } from './note';
import { BusItem } from './bus-item';


@Component({
  selector: 'app-bus-reports',
  templateUrl: './bus-reports.component.html',
  styleUrls: ['./bus-reports.component.scss']
})

export class BusReportsComponent implements OnInit {
  
  busReports : BusReport;
  openById = {};
  selectedBusNoteId : number = null;
  
  constructor(private busService: BusService) {

   }

  ngOnInit() {
    this.getBusReports();
  }

  getBusReports(): void {
    this.busService.getBusReports().subscribe(resp => {
      this.busReports = resp;
    });
  }

  onCancel(busItem : BusItem) :  void{
    busItem.note = null;
    this.selectedBusNoteId = null;
  }

  onSave(busItem : BusItem, pos:number) : void {

     //initialise the busNotes in busItem to an empty array as it is initially null
      if(!busItem.busNotes){
        this.busReports.data[pos].busNotes = new Array<BusNote>();
      }

      if(this.selectedBusNoteId === null){
        let busNoteId = this.busReports.data[pos].busNotes.length + 1;

        const busNote : BusNote = new BusNote(busNoteId, busItem.note, new Date());
     
        this.busReports.data[pos].busNotes.splice(0, 0, busNote);
      }else{
         let itemPos : number = -1;
         let busNote : BusNote = null;
         for(let i = 0; i < this.busReports.data[pos].busNotes.length; i++){
          busNote = this.busReports.data[pos].busNotes[i];
           
           if(busNote.noteId === this.selectedBusNoteId){
             busNote.note = busItem.note;
             busNote.modifiedDate = new Date();
             itemPos = i;
             break;
           }
         }

         if(busNote != null){
          //remove the busNote from its current position in the array as its been modified;
          this.busReports.data[pos].busNotes.splice(itemPos, 1);

          //push it to the top
          this.busReports.data[pos].busNotes.splice(0, 0, busNote);
         }
      }

      //re-initialise note and id
      busItem.note = null;
      this.selectedBusNoteId = null;
      
  }

  onChange(event : NgbPanelChangeEvent) : void {
    this.openById[event.panelId] = event.nextState;
  }

  updateNoteSelected(value:number): void {
    this.selectedBusNoteId = value;
  }

}
