import { BusData } from "./bus-data";
import { BusNote } from "./note";

export class BusItem {
    organisation:string;
    date:Date;
    busData: Array<BusData>;
    
    busNotes:Array<BusNote>;

    note : string;

    constructor() { }
}
