import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusReportDetailComponent } from './bus-report-detail.component';
import { CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA } from '@angular/core';

describe('BusReportDetailComponent', () => {
  let component: BusReportDetailComponent;
  let fixture: ComponentFixture<BusReportDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusReportDetailComponent ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusReportDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should make the first three characters of the route variant bold', () => {
      let variant : string = '891 2 1';

      const updatedVariant = component.getFormattedVariant(variant);

      expect(updatedVariant).toBe('<span><strong>891</strong></span> 2 1');

  });

  it('should not make the first three characters of the route variant bold', () => {
    let variant : string = 'UNKNOWN';

    const updatedVariant = component.getFormattedVariant(variant);

    expect(updatedVariant).toBe('UNKNOWN');

  });

  it('should return correct status for supplied routeDeviation', () => {
    
    let deviationFromTimetable : number = 77;
    expect(component.getStatus(deviationFromTimetable)).toBe('On Time');

    deviationFromTimetable = 340;
    expect(component.getStatus(deviationFromTimetable)).toBe('Late');

    deviationFromTimetable = -287;
    expect(component.getStatus(deviationFromTimetable)).toBe('Early');

    deviationFromTimetable = null;
    expect(component.getStatus(deviationFromTimetable)).toBe('Unknown');
   

  });

  
  
});
