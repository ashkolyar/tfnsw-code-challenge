import { Component, OnInit, Input } from '@angular/core';
import { BusItem } from '../bus-item';


@Component({
  selector: 'app-bus-report-detail',
  templateUrl: './bus-report-detail.component.html',
  styleUrls: ['./bus-report-detail.component.scss']
})
export class BusReportDetailComponent implements OnInit {

  @Input() busItem: BusItem;

  constructor() { }

  ngOnInit() {
  }

  getFormattedVariant(routeVariant: string): string {
    return routeVariant !== 'UNKNOWN' ? '<span><strong>' + routeVariant.substring(0, 3) + '</strong></span>' + routeVariant.substring(3) : routeVariant;
  }

  getStatus(deviationFromTimetable: number): string {

    if (deviationFromTimetable <= 0 && deviationFromTimetable !== null) {
      return 'Early';
    } else if (deviationFromTimetable > 0 && deviationFromTimetable <= 300) {
      return 'On Time';
    } else if (deviationFromTimetable > 300) {
      return 'Late';
    } else if (deviationFromTimetable === null) {
      return 'Unknown';
    }
  }

}
