import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BusReportsComponent } from './bus-reports/bus-reports.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { BusReportDetailComponent } from './bus-reports/bus-report-detail/bus-report-detail.component';
import { BusNotesComponent } from './bus-reports/bus-notes/bus-notes.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { ConfirmationDialogService } from './confirmation-dialog/confirmation-dialog.service';

@NgModule({
  declarations: [
    AppComponent,
    BusReportsComponent,
    BusReportDetailComponent,
    BusNotesComponent,
    ConfirmationDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, ReactiveFormsModule, 
    HttpClientModule,
    NgbModule.forRoot(),
    AngularFontAwesomeModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [ConfirmationDialogService],
  entryComponents: [ ConfirmationDialogComponent ],
  bootstrap: [AppComponent]
 
})
export class AppModule { }
