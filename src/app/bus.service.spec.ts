import { TestBed, inject } from '@angular/core/testing';

import { BusService } from './bus.service';
import { HttpClientModule } from '@angular/common/http';

describe('BusService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BusService],
      imports:[HttpClientModule]
    });
  });

  it('should be created', inject([BusService], (service: BusService) => {
    expect(service).toBeTruthy();
  }));
});
