import { BusItem } from "./bus-reports/bus-item";

export class BusReport {
    data: Array<BusItem>;
}