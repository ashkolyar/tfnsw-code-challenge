import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BusReport } from './bus.report';

@Injectable({
  providedIn: 'root'
})
export class BusService {

  constructor(private http: HttpClient) { }

  busReportDataUrl = 'assets/bus-services-data.json';


  getBusReports() {
    return this.http.get<BusReport>(this.busReportDataUrl);
  }
}
