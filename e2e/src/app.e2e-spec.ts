import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Bus Reports');
  });

  it('should display correct bus route title', () => {
    page.navigateTo();
    expect(page.getBusReportTitleForSydneyBus()).toEqual('Sydney Buses - 25/09/2015');

    expect(page.getBusReportTitleForWestBus()).toEqual('Westbus - 25/09/2015');
  });

  it('should display bus id', () => {
    page.navigateTo();
    expect(page.getBusIdOfFirstRecordForSydneyBus()).toEqual('42612');
  });

  it('should add note to notes', () => {
    page.navigateTo();
    expect(page.getAddedNote('test note 1')).toEqual('test note 1');
  });

  it('should update note', () => {
    page.navigateTo();
    expect(page.getUpdatedNote('test note 1','updated note')).toEqual('updated note');
  });
});
