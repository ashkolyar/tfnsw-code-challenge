import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }

  getBusReportTitleForSydneyBus() {
    return element(by.xpath('//*[@id="0-header"]/h5/button/div/div[1]/h5')).getText();
  }

  getBusReportTitleForWestBus() {
    return element(by.xpath('//*[@id="1-header"]/h5/button/div/div[1]/h5')).getText();
  }

  getBusIdOfFirstRecordForSydneyBus(){
    element(by.xpath('//*[@id="0-header"]/h5/button')).click();
    return element(by.xpath('//*[@id="0"]/app-bus-report-detail/div[2]/div[1]/div[1]')).getText();
  }

  getAddedNote(note : string){
    element(by.xpath('//*[@id="0-header"]/h5/button')).click();
    browser.driver.sleep(1000);
    element(by.xpath('//*[@id="0"]/div[1]/div/textarea')).sendKeys(note);
    browser.driver.sleep(1000);
    element(by.xpath('//*[@id="0"]/div[2]/div/button')).click();
    browser.driver.sleep(1000);

    return element(by.xpath('//*[@id="0"]/app-bus-notes/div/div/div[2]/div/div[1]')).getText();
  }

  getUpdatedNote(original : string, updated: string){
    element(by.xpath('//*[@id="0-header"]/h5/button')).click();
    element(by.xpath('//*[@id="0"]/div[1]/div/textarea')).sendKeys(original);
    element(by.xpath('//*[@id="0"]/div[2]/div/button')).click();

    browser.driver.sleep(1000);
    element(by.xpath('//*[@id="0"]/app-bus-notes/div/div/div[2]/div/div[4]/button[1]')).click();
    element(by.xpath('//*[@id="0"]/div[1]/div/textarea')).clear();
    element(by.xpath('//*[@id="0"]/div[1]/div/textarea')).sendKeys(updated);
    browser.driver.sleep(1000);
    element(by.xpath('//*[@id="0"]/div[2]/div/button')).click();
    browser.driver.sleep(1000);
    return element(by.xpath('//*[@id="0"]/app-bus-notes/div/div/div[2]/div/div[1]')).getText();
  }

}
